A simple little wrapper around ImageMagick montage to create a PDF of index
pages for your images. Specify the number of columns and rows, and then give a
list of files. Everything will be sized to fit a Letter page in portrait
ortientation as best as possible. A good number of cols and rows is 3x4.

Example:

$ mkimgindex 3 4 `ls -v ~/my-images/*.jpg`

Will always output to ${PWD}/montage.pdf, overwriting if necessary.
